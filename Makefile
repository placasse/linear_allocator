
CXX_FLAGS=--std=c++17

all: dg gfl tpv
	./dg
	./gfl
	./tpv

dg: Tests/dual_graph.cpp src/paged_linear_allocator.hpp Tests/primal_graph.hpp
	c++ -O3 -g $(CXX_FLAGS) -Isrc -lboost_system Tests/dual_graph.cpp -o dg

gfl: Tests/graph_forward_list.cpp src/paged_linear_allocator.hpp
	c++ -O3 -g $(CXX_FLAGS) -Isrc Tests/graph_forward_list.cpp -o gfl

tpv: Tests/triangles_per_vertex.cpp src/linear_allocator.hpp src/paged_linear_allocator.hpp
	c++ -O3 -g $(CXX_FLAGS) -Isrc -lboost_system Tests/triangles_per_vertex.cpp -o tpv

valgrind_dg: dg
	valgrind --tool=callgrind ./dg
	valgrind --tool=memcheck --leak-check=full ./dg

valgrind_tpv: tpv
	valgrind --tool=callgrind ./tpv
	valgrind --tool=memcheck ./tpv

valgrind_gfl: gfl
	valgrind --tool=callgrind ./gfl
	valgrind --tool=memcheck ./gfl

valgrind: valgrind_tpv valgrind_gfl

clean:
	rm -f dg gfl tpv

phony: clean all all_benches

