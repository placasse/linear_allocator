// This simple example use standard container with different allocator.
// 1: default std::new_allocator
// 2: linear_allocator
// 3: boost::mem_pool_allocator
//

#include "primal_graph.hpp"
#include "linear_allocator.hpp"
#include "paged_linear_allocator.hpp"
#include <chrono>
#include <ctime>
#include <forward_list>
#include <iomanip>
#include <iostream>
#include <numeric>
#include <vector>

#define BOOST_POOL_NO_MT
#include <boost/pool/object_pool.hpp>
#include <boost/pool/pool_alloc.hpp>

// Data for tests.
// Contains a list of triangles.
#include "chair.p"

template <class PointsList>
void t1(const int* triangles, const int nbTriangles, PointsList& points)
{
    FillTrianglesPerVertex(reinterpret_cast<const Eigen::Vector3i*>(triangles), nbTriangles, points);

    // Just do something with the points that validate the values are goods.
    // Compute the sum of the triangles over the vertices.
    // Each triangle being in three vertices, this must equals
    // \sum_{i=1}^{n} 3*i = n*(n+1)/2
    int nbTP = 0;
    int hash = 0;
    for (auto p : points)
        for (auto t : p)
        {
            ++nbTP;
            hash += t;
        }

    if (hash != (3 * nbTriangles * (nbTriangles - 1) / 2))
        throw new std::logic_error("Bad sum.");
}

// Using default std::new_allocator
void T1(int* triangles, int nbTriangles, int nbVertices, int nbTests)
{
    std::clock_t c_start = std::clock();
    typedef std::vector<std::forward_list<int>> TDPointsVV1;
    for (int i = 0; i < nbTests; ++i)
    {
        TDPointsVV1 points(nbVertices);
        t1(triangles, nbTriangles, points);
    }
    std::clock_t c_end = std::clock();
    std::cout << std::fixed << std::setprecision(2)
              << "T1 default std::new_allocator CPU time used: " << 1000.0 * (c_end - c_start) / CLOCKS_PER_SEC << " ms\n";
}

void T2(int* triangles, int nbTriangles, int nbVertice, int nbTests)
{
    std::clock_t c_start = std::clock();
    // Define a buffer and a unique tag for the allocator.
    // User should give a tight majoration of the memory needed.
    // Too large buffer waste memory, to small will throw an std::bad_alloc in the algorithm.
    const size_t buffer_size = 1024 * 1024;
    struct T2_buffer_tag
    {
    };
    typedef linear_allocator<int, T2_buffer_tag> TDallocator2;
    typedef std::forward_list<int, TDallocator2> TDFwdListInt2;
    typedef std::vector<TDFwdListInt2, TDallocator2> TDPointsVV2;

    // The buffer used to allocate all dynamic data of the algorithm.
    // Free this memory when the algorithm is done or keep it for the next time if you prefer.
    // If std::bad_alloc is thrown, this buffer can be increased.
    // However, this would invalidate any pointer inside it.
    std::vector<char> buffer(buffer_size);
    TDallocator2::SetBuffer(&buffer[0], buffer_size);

    TDPointsVV2* points = 0;
    for (int i = 0; i < nbTests; ++i)
    {
        points = TDallocator2::New<TDPointsVV2>(nbVertice);
        t1(triangles, nbTriangles, *points);
        // Uncomment PrintUsage to know the size actually used in the buffer.
        // This way you could tweak your formula for the buffer_size.
        //    TDallocator2::PrintUsage();
        TDallocator2::ResetBuffer();
        // Here the pointer 'points' is invalid, don't use it.
        // Destructor of points will never be called,
        // there is no leak because everything is allocated inside the linear_allocator.
    }
    std::clock_t c_end = std::clock();
    std::cout << std::fixed << std::setprecision(2)
              << "T2 linear_allocator CPU time used: " << 1000.0 * (c_end - c_start) / CLOCKS_PER_SEC << " ms\n";
}

void T3(int* triangles, int nbTriangles, int nbVertices, int nbTests)
{
    // TODO: There is no speedup in using fast_pool_allocator this way.
    //       What am-I doing wrong?
    //       Also, it's leaking memory.

    std::clock_t c_start = std::clock();
    typedef std::forward_list<int, boost::fast_pool_allocator<int>> TDFwdListInt3;
    typedef std::vector<TDFwdListInt3, boost::fast_pool_allocator<TDFwdListInt3>> TDPointsVV3;
    for (int i = 0; i < nbTests; ++i)
    {
        TDPointsVV3 points(nbVertices);
        t1(triangles, nbTriangles, points);
    }
    std::clock_t c_end = std::clock();
    std::cout << std::fixed << std::setprecision(2)
              << "T3 boost pool_allocator CPU time used: " << 1000.0 * (c_end - c_start) / CLOCKS_PER_SEC << " ms\n";
}

void T4(int* triangles, int nbTriangles, int nbVertices, int nbTests)
{
    std::clock_t c_start = std::clock();
    typedef std::forward_list<int, boost::fast_pool_allocator<int>> TDFwdListInt4;
    typedef std::vector<TDFwdListInt4, boost::fast_pool_allocator<TDFwdListInt4>> TDPointsVV4;
    struct TDPointsVV4_pool_tag
    {
    };
    typedef boost::singleton_pool<TDPointsVV4_pool_tag, sizeof(TDPointsVV4)> TDPointsVV4Alloc;
    for (int i = 0; i < nbTests; ++i)
    {
        // Seems there is no construct in this pool, I call new manually after this.
        TDPointsVV4* points = static_cast<TDPointsVV4*>(TDPointsVV4Alloc::malloc());
        points = new (points) TDPointsVV4(nbVertices);

        t1(triangles, nbTriangles, *points);

        // Purge all pools used.
        // I thought it would skip all the destructor and be faster, but it is not.
        boost::singleton_pool<boost::fast_pool_allocator_tag, sizeof(int)>::purge_memory();
        boost::singleton_pool<boost::pool_allocator_tag, sizeof(TDFwdListInt4)>::purge_memory();
        boost::singleton_pool<TDPointsVV4_pool_tag, sizeof(TDPointsVV4)>::purge_memory();
    }
    std::clock_t c_end = std::clock();
    std::cout << std::fixed << std::setprecision(2)
              << "T4 boost pool_allocator CPU time used: " << 1000.0 * (c_end - c_start) / CLOCKS_PER_SEC << " ms\n";
}

void T5(int* triangles, int nbTriangles, int nbVertice, int nbTests)
{
    std::clock_t c_start = std::clock();
    // Define a unique tag for the paged_linear_allocator.
    // User should give a tight majoration of the memory needed.
    // For this example, we intentionally choose a size too small,
    // just to test the allocation of subsequent pages.
    // As long as pagesize is not too small, it should not impact performance.
    // Of course, there could be a little memory waste at the end of each page
    // if big objects are allocated.
    struct T5_buffer_tag
    {
    };
    typedef paged_linear_allocator<int, T5_buffer_tag> TDallocator5;
    typedef std::forward_list<int, TDallocator5> TDFwdListInt5;
    typedef paged_linear_allocator<TDFwdListInt5, T5_buffer_tag> TDallocator3;
    typedef std::vector<TDFwdListInt5, TDallocator3> TDPointsVV5;

    // There is one list for each vertex, plus the nodes of the list.
    // The vector of list must have enough place into the first buffer.
    // This size will certainly be too small.
    // char needed (for the stl I have here):
    //  const size_t size_needed = 24 + nbVertice*8 + 6*nbTriangles*16;
    //  std::cout << "size I think I need = " << size_needed << std::endl;
    const size_t first_buffer_size = 24 + nbVertice * 8 + nbTriangles * 8;
    TDallocator5::SetSizes(first_buffer_size, nbTriangles * 16);

    TDPointsVV5* points = 0;
    for (int i = 0; i < nbTests; ++i)
    {
        TDallocator5::InvalidateAll();
        points = TDallocator5::New<TDPointsVV5>(nbVertice);
        t1(triangles, nbTriangles, *points);
        // Destructor of points will never be called,
        // there is no leak because everything is allocated inside the linear_allocator.
    }
    // Uncomment PrintUsage to know to size actually used in the buffer.
    // This way you could tweak your formula for the buffer_size.
    //  TDallocator5::PrintUsage();
    // Free all memory when you are totally done, not for each call of the algorithm.
    TDallocator5::FreeAll();
    std::clock_t c_end = std::clock();
    std::cout << std::fixed << std::setprecision(2)
              << "T5 paged_linear_allocator CPU time used: " << 1000.0 * (c_end - c_start) / CLOCKS_PER_SEC << " ms\n";
}

int main()
{
    // Number of time each test will be repeated.
    // For [paged_]linear_allocator, there is an overhead to allocate buffer[s].
    // This overhead become marginal if the memory is reused often.
    int nbTests = 20;

    // Test the allocator with different meshes.
    // Of course, in a real life software,
    // the idea is to reuse the same buffer for as many allocations as possible.

    {
        // A cube for debuging.

        // Each triangles have six indices : (vertex, normal) * 3 vertices
        int cube[] = {0, 0, 1, 1, 2, 2, 1, 1, 0, 0, 3, 3, 3, 3, 0, 0, 4, 4, 5, 4, 6, 0, 7, 3, 7, 3, 6, 0, 8, 1, 9, 2, 8, 1, 6, 0};
        int nbTriangles = 6;
        int nbVertice = 10;

        T1(cube, nbTriangles, nbVertice, nbTests);
        T2(cube, nbTriangles, nbVertice, nbTests);
        T3(cube, nbTriangles, nbVertice, nbTests);
        T4(cube, nbTriangles, nbVertice, nbTests);
        T5(cube, nbTriangles, nbVertice, nbTests);
    }

    // A chair.
    T1(p_chair, 2968, 50118, nbTests);
    T2(p_chair, 2968, 50118, nbTests);
    T3(p_chair, 2968, 50118, nbTests);
    T4(p_chair, 2968, 50118, nbTests);
    T5(p_chair, 2968, 50118, nbTests);
}
