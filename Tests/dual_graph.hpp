
//
// This simple example use standard container with different allocator.
// 1: default new_allocator
// 2: linear_allocator
// 3: boost::mem_pool_allocator
//

#include "forward_list_AllocS.hpp"
#include "linear_allocator.hpp"
#include "paged_linear_allocator.hpp"
#include "vector_AllocS.hpp"
#include <Eigen/Dense>
#include <chrono>
#include <ctime>
#include <forward_list>
#include <iomanip>
#include <iostream>
#include <numeric>
#include <vector>

#define BOOST_POOL_NO_MT
#include <boost/pool/object_pool.hpp>
#include <boost/pool/pool_alloc.hpp>

// Data for tests.
// Contains a list of triangles.
#include "chair.p"

template <typename TriangleList>
void PreconditionForLookForNeighbour(const TriangleList& triangles, int triangleOrigin)
{
    bool triangleOrigineFound = false;
    // TODO std::numeric<int>::max
    //  int previousT = std::numeric<int>::max;
    int previousT = 999999;
    for (int noT : triangles)
    {
        if (noT >= previousT)
            throw std::logic_error("triangles should be strickly sorted (descending order).");

        if (noT == triangleOrigin)
            triangleOrigineFound = true;

        previousT = noT;
    }

    if (!triangleOrigineFound)
        throw std::logic_error("triangles should contains triangleOrigin.");
}

// Return a triangle, with an index strickly greater than triangleOrigin, that have both vertex0 and vertex1.
// Return -1 if can't find one.
// Precondition: triangles in lists are ordered in descending order (because we pushed forward).
// Precondition: each list contains triangleOrigin.
// Limitation: The case where there is more than one valid triangle to find is not handle; only one triangle is returned.
template <class TrianglesList>
int LookForNeighbour(const TrianglesList& triangles0, const TrianglesList& triangles1, int triangleOrigin)
{
#ifdef DEBUG
    PreconditionForLookForNeighbour(triangles0, triangleOrigin);
    PreconditionForLookForNeighbour(triangles1, triangleOrigin);
#endif

    // Is there any intersection between the list of triangles for vertices 0 and 1 (except for triangleOrigin)?

    // Iterate throught triangles of both vertices.
    // Each iterator will reach triangleOrigin before their end,
    // so we don't need to compare with end().

    // Note: TrianglesList are small lists. Vertices are shared usually by about 6 triangles.
    //       Linear search is as good (or better) as binary search.

    auto iter0 = triangles0.begin();
    auto iter1 = triangles1.begin();
    if (*iter0 == triangleOrigin || *iter1 == triangleOrigin)
        return -1;

    // Here we don't know which of *iter0 and *iter1 is the greater.
    while (*iter0 < *iter1)
    {
        ++iter1;
        if (*iter1 == triangleOrigin)
            return -1;
    }

    // Here *iter0 >= *iter1
    if (*iter0 == *iter1)
        return *iter0;

    do
    {
        // Here *iter0 > *iter1
        do
        {
            ++iter0;
            if (*iter0 == triangleOrigin)
                return -1;
        } while (*iter0 > *iter1);

        // Here *iter0 <= *iter1
        if (*iter0 == *iter1)
            return *iter0;

        // Here *iter0 < *iter1
        do
        {
            ++iter1;
            if (*iter1 == triangleOrigin)
                return -1;
        } while (*iter0 < *iter1);

        // Here *iter0 >= *iter1
        if (*iter0 == *iter1)
            return *iter0;

    } while (true);
}

// Return true if an edge is added,
// else otherwise.
template <class TrianglesList, class Graph>
bool MayBeAddThisEdgeToDualGraph(const TrianglesList& triangles0, const TrianglesList& triangles1, int triangleOrigin,
                                 Graph& dual_graph)
{
    int otherTriangle = LookForNeighbour(triangles0, triangles1, triangleOrigin);
    if (otherTriangle != -1)
    {
        add_edge(triangleOrigin, otherTriangle, dual_graph);
        return true;
    }
    else
    {
        // If needed, we could add this edge to a list of border edges
        return false;
    }
}

// Create the dual graph of the mesh.
// Each triangle becomes a vertex.
// Edges shared by two triangles becomes ... edges!
template <int IndicesPerTriangles, class TrianglesPerVertex, class DualGraph>
void FillDualGraph(const Eigen::Matrix<int, IndicesPerTriangles, 1>* triangles, int nbTriangles, const TrianglesPerVertex& points,
                   DualGraph& dual_graph)
{
    constexpr int offset = IndicesPerTriangles / 3;

    // Avoid useless reallocation of the vertices.
    // Nice that we can access this sensitive internal attribute publicly.
    dual_graph.m_vertices.reserve(nbTriangles);

    dual_graph.clear();

    for (int i = 0; i < nbTriangles; ++i)
        add_vertex(dual_graph);

    for (int i = 0; i < nbTriangles; ++i)
    {
        // The triangle has three edges.
        // For each edge, look if it is shared by another triangle.
        // If so, add an edge to the dual graph.
        const auto& triangle = triangles[i];
        int vertex0 = triangle[0 * offset];
        int vertex1 = triangle[1 * offset];
        int vertex2 = triangle[2 * offset];
        const auto& triangles0 = points[vertex0];
        const auto& triangles1 = points[vertex1];
        const auto& triangles2 = points[vertex2];

        MayBeAddThisEdgeToDualGraph(triangles0, triangles1, i, dual_graph);
        MayBeAddThisEdgeToDualGraph(triangles1, triangles2, i, dual_graph);
        MayBeAddThisEdgeToDualGraph(triangles2, triangles0, i, dual_graph);
    }
}

// Fill the dual_graph of the meshes.
// than do something with it.
// points contains a vector of vertices.
// Each vertex has the list of its triangles.
template <class TrianglesPerVertex, class Graph>
void Tgraph(const int* triangles, int nbTriangles, const TrianglesPerVertex& points, Graph& dual_graph)
{
    FillDualGraph(reinterpret_cast<const Eigen::Matrix<int, 6, 1>*>(triangles), nbTriangles, points, dual_graph);
    // Avoid useless reallocation of the vertices.
    // Nice that we can access this sensitive internal attribute publicly.
    dual_graph.m_vertices.reserve(nbTriangles);

    // Create the dual graph of the mesh.
    // Each triangle becomes a vertex.
    // Edges shared by two triangles becomes ... edges!
    //
    // For each triangles, find its neighbours.
    //
    bool closedSurface = true;
    for (int i = 0; i < nbTriangles; ++i)
    {
        // The triangle has three edges.
        // For each edge, look if it is shared by another triangle.
        // If so, add an edge to the dual graph.
        // If a triangle share no edge, it must add a vertex.
        int vertex0 = triangles[6 * i + 2 * 0];
        int vertex1 = triangles[6 * i + 2 * 1];
        int vertex2 = triangles[6 * i + 2 * 2];
        bool anEdgeIsAdded = false;
        bool allEdgesAreAdded = true;
        bool thisEdgeIsAdded = true;
        const auto& triangles0 = points[vertex0];
        const auto& triangles1 = points[vertex1];
        const auto& triangles2 = points[vertex2];

        thisEdgeIsAdded = MayBeAddThisEdgeToDualGraph(triangles0, triangles1, i, dual_graph);
        allEdgesAreAdded &= thisEdgeIsAdded;
        anEdgeIsAdded |= thisEdgeIsAdded;
        thisEdgeIsAdded = MayBeAddThisEdgeToDualGraph(triangles1, triangles2, i, dual_graph);
        allEdgesAreAdded &= thisEdgeIsAdded;
        anEdgeIsAdded |= thisEdgeIsAdded;
        thisEdgeIsAdded = MayBeAddThisEdgeToDualGraph(triangles2, triangles0, i, dual_graph);
        allEdgesAreAdded &= thisEdgeIsAdded;
        anEdgeIsAdded |= thisEdgeIsAdded;
        closedSurface &= allEdgesAreAdded;
    }

    // Now that we have the dual graph of the mesh,
    // do something with it.
    // TODO, compute connected components.
}

