#include "dual_graph.hpp"
#include "linear_allocator.hpp"
#include <boost/test/unit_test.hpp>
#include <iostream>
#include <vector>

BOOST_AUTO_TEST_CASE(linear_allocator_dual_graph_cube)
{
    // Each triangles have six indices : (vertex, normal) * 3 vertices
    //int cube[] = {0, 0, 1, 1, 2, 2, 1, 1, 0, 0, 3, 3, 3, 3, 0, 0, 4, 4, 5, 4, 6, 0, 7, 3, 7, 3, 6, 0, 8, 1, 9, 2, 8, 1, 6, 0};
    //int nbTriangles = 6;
    //int nbVertice = 10;

    struct buffer_tag
    {
    };
    typedef linear_allocator<int, buffer_tag> allocator_type;
    typedef std::forward_list<int, allocator_type> forward_list_int;

    // The same allocator can be used to allocate different type (using rebind).
    typedef std::vector<forward_list_int, allocator_type> vector_type;

    const size_t buffer_size = 1024 * 1024;
    std::vector<char> buffer(buffer_size);
    allocator_type::SetBuffer(&buffer[0], buffer_size);

    vector_type v;
    for (int i = 0; i < 1024; ++i)
    {
        v.emplace_back();
        auto& l = v.back();
        for (int j = 0; j < (i/24); ++j)
            l.push_front(i);
    }

    // Notice that the used memory showed is creater than the final vector size.
    // When the vector move its content, no deallocation is done by the linear_allocator.
    allocator_type::PrintUsage(std::cout);
}

