#include "linear_allocator.hpp"
#include <boost/test/unit_test.hpp>
#include <iostream>
#include <vector>

BOOST_AUTO_TEST_CASE(linear_allocator_1)
{
    struct buffer_tag
    {
    };
    typedef linear_allocator<int, buffer_tag> allocator_type;
    typedef std::vector<int, allocator_type> vector_type;

    const size_t buffer_size = 1024 * 1024;
    std::vector<char> buffer(buffer_size);
    allocator_type::SetBuffer(&buffer[0], buffer_size);

    vector_type v;
    for (int i = 0; i < 1024; ++i)
        v.push_back(i);

    // Notice that the used memory showed is creater than the final vector size.
    // When the vector move its content, no deallocation is done by the linear_allocator.
    allocator_type::PrintUsage(std::cout);
}

class A
{
  public:
    int m_a = 0;
    float m_b = 0;
    A(int a, float b) : m_a(a), m_b(b) {}
};

BOOST_AUTO_TEST_CASE(linear_allocator_class_constructor)
{
    struct buffer_tag
    {
    };
    typedef linear_allocator<A, buffer_tag> allocator_type;
    typedef std::list<A, allocator_type> list_type;

    const size_t buffer_size = 1024 * 1024;
    std::vector<char> buffer(buffer_size);
    allocator_type::SetBuffer(&buffer[0], buffer_size);

    list_type l;
    for (int i = 0; i < 1024; ++i)
        l.emplace_back(i, i * 2);

    int i = 0;
    for (auto& a : l)
    {
        BOOST_TEST(a.m_a == i);
        BOOST_TEST(a.m_b == i * 2);
        ++i;
    }

    while (!l.empty())
        l.pop_back();

    // Notice that the used memory showed is creater than the final vector size.
    // When the vector move its content, no deallocation is done by the linear_allocator.
    allocator_type::PrintUsage(std::cout);
}

BOOST_AUTO_TEST_CASE(linear_allocator_bad_alloc)
{
    struct buffer_tag
    {
    };
    typedef linear_allocator<int, buffer_tag> allocator_type;

    const size_t buffer_size = 2;
    std::vector<char> buffer(buffer_size);
    allocator_type::SetBuffer(&buffer[0], buffer_size);

    // 2 bytes available, asking for an int (4 bytes).
    BOOST_CHECK_THROW(allocator_type::New<int>(), std::bad_alloc);
}
