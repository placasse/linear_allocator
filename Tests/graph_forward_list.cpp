
//
// This is an example of using forward_list with boost::graph
// and the paged_linear_allocator.
//

#include "forward_list_AllocS.hpp"
#include "vector_AllocS.hpp"
#include "paged_linear_allocator.hpp"

// Test
int main(int, char*[]) {

  struct T5_buffer_tag {};
  typedef paged_linear_allocator<int, T5_buffer_tag> TDallocator5;
  typedef boost::forward_list_AllocS<TDallocator5> TD_falS;
  typedef boost::vector_AllocS<TDallocator5> TD_vS;
  typedef boost::adjacency_list<TD_falS, TD_vS, boost::directedS, boost::no_property, boost::no_property, boost::no_property, TD_falS> Graph;

  TDallocator5::SetSizes(1024, 1024);
  Graph g;
  boost::add_edge(1, 2, g);
}
