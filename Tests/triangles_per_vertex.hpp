#pragma once

#include <Eigen/Dense>
#include <forward_list>

template <class L>
void Add(L& l, int i)
{
    l.push_back(i);
}

template <class Alloc>
void Add(std::forward_list<int, Alloc>& l, int i)
{
    l.push_front(i);
}

// Each triangle is defined by three indices (its vertices).
// Input: A triangles list that could comes from a COLLADA files.
//        If IndicesPerTriangles is 6, a triangle is a list a indices ordored like (position, normal, position, normal, position, normal).
// Output: For vertex, the list of triangles where this vertex is used.
template <class TrianglesPerVertex, int IndicesPerTriangle>
void FillTrianglesPerVertex(const Eigen::Matrix<int, IndicesPerTriangle, 1>* triangles, const int nbTriangles, TrianglesPerVertex& points)
{
    const int offset = IndicesPerTriangle / 3;
    for (int i = 0; i < nbTriangles; ++i)
    {
        const auto& triangle = triangles[i];
        for (int j = 0; j < 3; ++j)
        {
            Add(points[triangle[j * offset]], i);
        }
    }
}

