
For some algorithms, having unexpectedly bad performance, valgrind or other profiler may show that malloc and free are consuming too much time.
In such cases, a better memory allocation scheme should be considered.

Suppose the following scenario where a lots of small objects need to be allocated, used together for some time, and then are all released.
In this situation, one can imagine allocating the memory for all those objects in only one big malloc, and then freeing all of them with just one big free, virtually zeroing the malloc/free time.

People asking for this feature in the Internet often refer to video games.
When entering a level a bunch of objects are created that live for the entire level and are all destroyed at the end.
I'm not sure if the allocation here is really significative, but it could be an appropriate use case.
A better one would be any algorithm that use a huge amount of small temporary objects (like a list of lists of numbers).

This project presents two allocators: linear_allocator and paged_linear_allocator.
They are std::allocator that linearly allocates into a given buffer and never deallocate.
When the allocators are resetted, their memory can be used again.
They are stateless allocator, the references to their buffers are static member.
It is still possible to use as many buffer as needed by using an appropriate tag template parameter.
They can be used directly with the standard containers from the stl.

The linear_allocator uses a buffer given by the user and throw an std::bad_alloc if it needs more.
The paged_linear_allocator allocate its buffer himself, at a size dictates by the user, but can also allocate other pages if it needs more place.

This project contains two header files: linear_allocator.h and paged_linear_allocator.
There is nothing to build or to link, just include one of them.

An example file is also provided which can be used as an oversimple benchmark.
In this example, a mesh is read as a list of triangles.
A vector contains, for each vertex, a list of the triangles that share this vertex.
Compiling with -O3 and timing (or executing with valgrind) shows a net gain with both of our allocators.
Commands to compile and launch valgring are in the simple Makefile included.
This example is a good use case, because the allocation was dominating the effective calculation.

Here are some other similar allocators that helped me write this one and could be usefull for other people reading about this topic:
__gnu_cxx::array_allocator,
stack_allocator by Charles Salvia
android::LinearAllocator

There is also other allocators in the market, based on other ideas:
boost pool_alloc et al.
other __gnu_cxx allocators
msdn allocators
https://github.com/tempesta-tech/blog/blob/master/mempool/pool.cc

# Building UnitTests

$ mkdir build_Debug
$ cd build_Debug
$ cmake .. -DCMAKE_BUILD_TYPE=Debug
$ make
$ UnitTests/UnitTests
$ valgrind --tool=memcheck --leak-check=full Tests/UnitTests


$ mkdir build_Release
$ cd build_Release
$ cmake .. -DCMAKE_BUILD_TYPE=Release
$ make
$ UnitTests/UnitTests
$ valgrind --tool=memcheck --leak-check=full Tests/UnitTests
