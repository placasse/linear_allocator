#pragma once

// Include this file to use
// a boost graph with its edges into a forward_list using paged_linear_allocator.

#include <forward_list>

namespace boost { namespace graph_detail {
  // Forward declaration, to avoid this error by clang :
  // 'container_category' should be declared prior to the call site
  // This must be before the include of boost.
  struct front_insertion_sequence_tag;
  template <class T, class Alloc>
    front_insertion_sequence_tag container_category(const std::forward_list<T, Alloc>&);
} }

#include <boost/graph/graph_traits.hpp>
#include <boost/graph/adjacency_list.hpp>

namespace boost { namespace graph_detail {

  template <class T, class Alloc>
      front_insertion_sequence_tag container_category(const std::forward_list<T, Alloc>&){
        return front_insertion_sequence_tag();
      }

  template <class T, class Alloc>
      stable_tag iterator_stability(const std::forward_list<T, Alloc>&) {
        return stable_tag();
      }

  template <class T, class Alloc>
      struct container_traits< std::forward_list<T, Alloc> > {
        typedef front_insertion_sequence_tag category;
        typedef stable_tag iterator_stability;
      };
} // namespace graph_detail

template <class Alloc>
struct forward_list_AllocS { };

template <class Alloc>
struct parallel_edge_traits<forward_list_AllocS<Alloc>>
{
  typedef allow_parallel_edge_tag type;
};

template <class Alloc, class ValueType>
struct container_gen<forward_list_AllocS<Alloc>, ValueType>
{
  typedef typename Alloc::template rebind<ValueType>::other Allocator;
  typedef std::forward_list<ValueType, Allocator> type;
};

} // namespace boost

