#pragma once

#include <functional>
#include <memory>
#include <sstream>
//#include <iostream>

template <class T, class buffer_tag>
class linear_allocator;

template <class buffer_tag>
class linear_allocator_base
{
  public:
    // This buffer is not deleted by, linear_allocator_base.
    // The one who allocates buffer is responsible for deleting it when it is no longer needed.
    static void SetBuffer(char* buffer, size_t buffer_size)
    {
        s_buffer_size = buffer_size;
        s_begin = buffer;
        s_end = buffer + s_buffer_size;
        s_stack_pointer = buffer;
    }

    static size_t capacity() { return s_buffer_size; }

    static void ResetBuffer() { s_stack_pointer = s_begin; }

    template <class OStream>
    static void PrintUsage(OStream& o)
    {
        size_t used = std::distance(s_begin, s_stack_pointer);
        size_t free = std::distance(s_stack_pointer, s_end);
        o << "buffer_tag = " << typeid(buffer_tag).name() << " s_buffer_size = " << s_buffer_size << " used = " << used
          << " free = " << free << " " << (100 * (double)used / (used + free)) << "%" << std::endl;
    }

    // Allocate space and constrcut an object of type T.
    template <typename T, typename... Args>
    static T* New(Args&&... args);

  protected:
    // Those are shared by all allocator with the same buffer_tag.
    // So, many lists may use the same buffer to allocate.
    static char* s_begin;
    static char* s_end;
    static char* s_stack_pointer;
    static size_t s_buffer_size;
};

//
// Change the buffer_tag for each new buffer.
// So the allocator instance doesn't need to hold a pointer to the buffer.
//
template <class T, class buffer_tag>
class linear_allocator : public linear_allocator_base<buffer_tag>
{
    using linear_allocator_base<buffer_tag>::s_begin;
    using linear_allocator_base<buffer_tag>::s_end;
    using linear_allocator_base<buffer_tag>::s_stack_pointer;
    using linear_allocator_base<buffer_tag>::s_buffer_size;

  public:
    typedef typename std::allocator<T>::value_type value_type;
    typedef typename std::allocator<T>::pointer pointer;
    typedef typename std::allocator<T>::const_pointer const_pointer;
    typedef typename std::allocator<T>::reference reference;
    typedef typename std::allocator<T>::const_reference const_reference;
    typedef typename std::allocator<T>::size_type size_type;
    typedef typename std::allocator<T>::difference_type difference_type;

  public:
    explicit linear_allocator() {}

    template <class U>
    linear_allocator(const linear_allocator<U, buffer_tag>& /*other*/)
    {
    }

    size_t max_size() const noexcept { return std::distance(s_stack_pointer, s_end) / sizeof(value_type); }

    pointer allocate(size_type n)
    {
        size_type nchar = sizeof(T) * n;

        const size_t l_available = std::distance(s_stack_pointer, s_end);

        if (nchar > l_available)
        {
            if (s_begin == 0)
            {
                std::stringstream s;
                s << "Must call SetBuffer for this allocator type linear_allocator<" << s_buffer_size << ", "
                  << typeid(buffer_tag).name() << ">";
                throw std::logic_error(s.str());
            }

            throw std::bad_alloc();
        }

        pointer result = reinterpret_cast<T*>(s_stack_pointer);
        s_stack_pointer += nchar;
        return result;
    }

    void deallocate(pointer, size_type)
    {
        // Do nothing.
        // This allocator never free the space.
        // Instead, you must reset the buffer when you're are done.
    }

    template <typename U, typename... Args>
    void construct(U* p, Args&&... args)
    {
        ::new ((void*)p) U(std::forward<Args>(args)...);
    }

    template <typename U>
    void destroy(U* /*p*/)
    {
        // Be aware, destructors are not called!
        // This is not Kasher.
    }

    pointer address(reference x) const noexcept { return std::addressof(x); }

    const_pointer address(const_reference x) const noexcept { return std::addressof(x); }

    template <class U>
    struct rebind
    {
        typedef linear_allocator<U, buffer_tag> other;
    };
};

template <class T1, class buffer_tag, class T2>
constexpr bool operator==(const linear_allocator<T1, buffer_tag>& lhs, const linear_allocator<T2, buffer_tag>& rhs) noexcept
{
    return true;
}

template <class T1, class buffer_tag, class T2>
constexpr bool operator!=(const linear_allocator<T1, buffer_tag>& lhs, const linear_allocator<T2, buffer_tag>& rhs) noexcept
{
    return false;
}

template <class buffer_tag>
char* linear_allocator_base<buffer_tag>::s_begin = nullptr;
template <class buffer_tag>
char* linear_allocator_base<buffer_tag>::s_end = nullptr;
template <class buffer_tag>
char* linear_allocator_base<buffer_tag>::s_stack_pointer = nullptr;
template <class buffer_tag>
size_t linear_allocator_base<buffer_tag>::s_buffer_size = 0;

// namespace std
//{
// template <typename T, class buffer_tag>
// struct allocator_traits<linear_allocator<T, buffer_tag>>
//{
//    typedef linear_allocator<T, buffer_tag> allocator_type;
//
//    typedef typename allocator_type::value_type value_type;
//    typedef typename allocator_type::pointer pointer;
//    typedef typename allocator_type::const_pointer const_pointer;
//    typedef typename allocator_type::reference reference;
//    typedef typename allocator_type::const_reference const_reference;
//    typedef typename allocator_type::size_type size_type;
//    typedef typename allocator_type::difference_type difference_type;
//
//    template <typename T2>
//    using rebind_alloc = linear_allocator<T2, buffer_tag>;
//
//    template <typename T2>
//    using rebind_traits = allocator_traits<rebind_alloc<T>>;
//
//    [[nodiscard]] static pointer allocate(allocator_type& a, size_type n) { return a.allocate(n); }
//
//    static void deallocate(allocator_type& a, pointer p, size_type n) { a.deallocate(p, n); }
//
//    static size_type max_size(const allocator_type& a) noexcept { return a.max_size(); };
//
//    template <typename U, typename... Args>
//    static void construct(allocator_type& a, U* p, Args&&... args)
//    {
//        a.construct(p, std::forward<Args>(args)...);
//    }
//
//    template <typename U>
//    static void destroy(allocator_type& a, U* p)
//    {
//        a.destroy(p);
//    }
//};
//
//} // namespace std

template <class buffer_tag>
template <typename T, class... Args>
T* linear_allocator_base<buffer_tag>::New(Args&&... args)
{
    typedef linear_allocator<T, buffer_tag> allocator_type;
    allocator_type a;
    T* ptr = a.allocate(1);
    a.construct(ptr, std::forward<Args>(args)...);
    return ptr;
}

