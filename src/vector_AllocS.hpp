#pragma once

// Include this file to use
// a boost graph with its edges into a vector using paged_linear_allocator.

#include <vector>

#include <boost/graph/graph_traits.hpp>
#include <boost/graph/adjacency_list.hpp>

namespace boost {
template <class Alloc>
struct vector_AllocS { };

template <class Alloc>
struct parallel_edge_traits<vector_AllocS<Alloc>>
{
  typedef allow_parallel_edge_tag type;
};

template <class Alloc, class ValueType>
struct container_gen<vector_AllocS<Alloc>, ValueType>
{
  typedef typename Alloc::template rebind<ValueType>::other Allocator;
  typedef std::vector<ValueType, Allocator> type;
};

namespace detail {
  // Without this one, add_edge by edge indices won't work.
  template <class Alloc>
      struct is_random_access<vector_AllocS<Alloc>> {
        enum { value = true };
        typedef mpl::true_ type;
      };
} // namespace detail

} // namespace boost

