#ifndef INCLUDE_PAGED_LINEAR_ALLOCATOR_HPP
#define INCLUDE_PAGED_LINEAR_ALLOCATOR_HPP

#include <functional>
#include <iostream>
#include <list>
#include <memory>
#include <sstream>
#include <vector>

//
// This allocator first create a buffer of the size determine by the user.
// It also has the hability to expand its memory pool by adding page of a given size.
//

template <class buffer_tag>
class paged_linear_allocator_base
{
  public:
    // Set the size of the first buffer and of the subsequent pages.
    // Allocate the memory for the first buffer.
    // This function must be called once before allocating any memory.
    // It can be called more than one time,
    // but calling it after memory allocation will reset all the buffers,
    // thus invalidating any data and pointers inside it (internal memory could move).
    static void SetSizes(size_t first_buffer_size, size_t page_size)
    {
        s_buffer_list.clear();
        AllocateNewPage(first_buffer_size);
        s_wasted_memory = 0;
        s_first_buffer_size = first_buffer_size;
        s_page_size = page_size;
    }

    static void AllocateNewPage(size_t p_size)
    {
        s_buffer_list.push_back(std::vector<char>());
        auto l_last = --(s_buffer_list.end());
        l_last->resize(p_size);
        InitPointer(l_last);
    }

    // Set back the pointer to the beginning of the first buffer.
    // Do not call any destructor, do not free any memory.
    // All data previously allocated are now considered invalid.
    // Next allocations will reuse the same memory.
    static void InvalidateAll()
    {
        if (s_buffer_list.empty())
            return;

        InitPointer(s_buffer_list.begin());
        s_wasted_memory = 0;
    }

    // Free all the buffers.
    // Do not call any destructor.
    // To be called when you are totally done with this allocator.
    static void FreeAll()
    {
        if (s_buffer_list.empty())
            return;

        s_buffer_list.clear();
        s_present_buffer = s_buffer_list.end();
    }

    static void PrintUsage()
    {
        std::cout << "PrintUsage buffer tag = " << typeid(buffer_tag).name() << " first buffer size = " << s_first_buffer_size
                  << " page size = " << s_page_size;
        if (s_buffer_list.empty())
        {
            std::cout << " is empty.";
            return;
        }
        size_t nbBuffers = 0;
        // Memory used in the last buffer.
        size_t used = std::distance(s_present_buffer->begin(), s_stack_pointer);
        // Plus all the memory from all the buffers.
        for (auto buffer : s_buffer_list)
        {
            ++nbBuffers;
            used += buffer.size();
        }
        // Minus what is wasted and the memory from the last buffer counted twice.
        used -= (s_buffer_list.back().size() + s_wasted_memory);

        size_t free = std::distance(s_stack_pointer, s_end);
        std::cout << " nbBuffers = " << nbBuffers << " used = " << used << " free = " << free << " "
                  << (100 * (double)used / (used + free)) << "%"
                  << " wasted = " << s_wasted_memory << std::endl;
    }

    // Allocate n chars.
    static char* allocate_nchar(size_t nchar)
    {
        const size_t l_available = std::distance(s_stack_pointer, s_end);
        if (nchar > l_available)
        {
            if (s_page_size == 0)
            {
                std::stringstream s;
                s << "Must call SetSizes for this allocator type paged_linear_allocator_base<" << typeid(buffer_tag).name()
                  << ">";
                throw std::logic_error(s.str());
            }

            // The present buffer is full, choose next or allocate a new one.
            s_wasted_memory += l_available;
            if (nchar > s_page_size)
            {
                std::cerr << "Need = " << nchar << " available = " << l_available << " now wasted = " << s_wasted_memory
                          << std::endl;
                // Allocating too much data, it won't fit into a page.
                // We could try to allocate a bigger page for this object,
                // but it is a problem in the user choice of allocation that he should corrected.
                throw new std::bad_alloc();
            }
            else
            {
                // Is there another page available?
                ++s_present_buffer;
                if (s_present_buffer == s_buffer_list.end())
                    AllocateNewPage(s_page_size);
                else
                    InitPointer(s_present_buffer);
            }
        }

        // Here we know there is enough space for this allocation.
        char* result = &*s_stack_pointer;
        s_stack_pointer += nchar;
        return result;
    }

    // Allocate space and an object of type T.
    template <typename T, typename... Args>
    static T* New(Args&&... args);

  protected:
    //
    // Those are shared by all allocator with the same buffer_tag.
    // So, many lists may use the same buffer to allocate.
    //
    static size_t s_first_buffer_size;
    static size_t s_page_size;

    // Buffers are allocated using usual malloc.
    typedef std::vector<char> TDBuffer;
    typedef std::list<TDBuffer> TDBuffers;
    static TDBuffers s_buffer_list;

    // Next allocation's data.
    static TDBuffers::iterator s_present_buffer;
    static TDBuffer::iterator s_stack_pointer;
    static TDBuffer::iterator s_end;

    // Just for stats.
    static size_t s_wasted_memory;

  private:
    static void InitPointer(TDBuffers::iterator p_buffer)
    {
        s_present_buffer = p_buffer;
        s_stack_pointer = s_present_buffer->begin();
        s_end = s_present_buffer->end();
    }
};

//
// Change the buffer_tag for each new buffer.
// So the allocator instance doesn't need to hold a pointer to the buffer.
//
template <class T, class buffer_tag>
class paged_linear_allocator : public paged_linear_allocator_base<buffer_tag>
{
    using paged_linear_allocator_base<buffer_tag>::s_stack_pointer;
    using paged_linear_allocator_base<buffer_tag>::s_end;
    using paged_linear_allocator_base<buffer_tag>::s_first_buffer_size;
    using paged_linear_allocator_base<buffer_tag>::s_page_size;
    using paged_linear_allocator_base<buffer_tag>::s_wasted_memory;

  public:
    typedef typename std::allocator<T>::value_type value_type;
    typedef typename std::allocator<T>::pointer pointer;
    typedef typename std::allocator<T>::const_pointer const_pointer;
    typedef typename std::allocator<T>::reference reference;
    typedef typename std::allocator<T>::const_reference const_reference;
    typedef typename std::allocator<T>::size_type size_type;
    typedef typename std::allocator<T>::difference_type difference_type;

  public:
    template <class U>
    struct rebind
    {
        typedef paged_linear_allocator<U, buffer_tag> other;
    };

    paged_linear_allocator() noexcept {}

    paged_linear_allocator(const paged_linear_allocator&) noexcept {}

    template <class U>
    paged_linear_allocator(const paged_linear_allocator<U, buffer_tag>& other) noexcept
    {
    }

    ~paged_linear_allocator() noexcept {}

    pointer address(reference x) const noexcept { return std::addressof(x); }

    const_pointer address(const_reference x) const noexcept { return std::addressof(x); }

    // Allocate enough space for n objects of type T.
    static pointer allocate(size_type n)
    {
        size_t nchar = n * sizeof(T);
        pointer result = reinterpret_cast<T*>(paged_linear_allocator_base<buffer_tag>::allocate_nchar(nchar));
        return result;
    }

    void deallocate(pointer /*p*/, size_type /*n*/)
    {
        // Do nothing.
        // This allocator never free the space.
        // Instead, you must reset the buffer when you're are done.
    }

    template <typename U, typename... Args>
    static void construct(U* p, Args&&... args)
    {
        std::allocator<T>().construct(p, std::forward<Args>(args)...);
    }

    template <class U>
    void destroy(U* p)
    {
        std::allocator<T>().destroy(p);
    }
};

template <class T1, class buffer_tag, class T2>
constexpr bool operator==(const paged_linear_allocator<T1, buffer_tag>& lhs,
                          const paged_linear_allocator<T2, buffer_tag>& rhs) noexcept
{
    return true;
}

template <class T1, class buffer_tag, class T2>
constexpr bool operator!=(const paged_linear_allocator<T1, buffer_tag>& lhs,
                          const paged_linear_allocator<T2, buffer_tag>& rhs) noexcept
{
    return false;
}

template <class buffer_tag>
size_t paged_linear_allocator_base<buffer_tag>::s_first_buffer_size = 0;

template <class buffer_tag>
size_t paged_linear_allocator_base<buffer_tag>::s_page_size = 0;

template <class buffer_tag>
typename paged_linear_allocator_base<buffer_tag>::TDBuffers paged_linear_allocator_base<buffer_tag>::s_buffer_list;

template <class buffer_tag>
typename paged_linear_allocator_base<buffer_tag>::TDBuffers::iterator paged_linear_allocator_base<buffer_tag>::s_present_buffer;

template <class buffer_tag>
typename paged_linear_allocator_base<buffer_tag>::TDBuffer::iterator paged_linear_allocator_base<buffer_tag>::s_stack_pointer;

template <class buffer_tag>
typename paged_linear_allocator_base<buffer_tag>::TDBuffer::iterator paged_linear_allocator_base<buffer_tag>::s_end;

template <class buffer_tag>
size_t paged_linear_allocator_base<buffer_tag>::s_wasted_memory = 0;

template <class buffer_tag>
template <typename T, class... Args>
T* paged_linear_allocator_base<buffer_tag>::New(Args&&... args)
{
    typedef paged_linear_allocator<T, buffer_tag> TDAlloc;
    T* ptr = TDAlloc::allocate(1);
    TDAlloc::construct(ptr, std::forward<Args>(args)...);
    return ptr;
}

#endif
